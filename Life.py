""" Code example from Complexity and Computation, a book about
exploring complexity science with Python.  Available free from

http://greenteapress.com/complexity

Copyright 2011 Allen B. Downey.
Distributed under the GNU General Public License at gnu.org/licenses/gpl.html.
"""

import numpy
import scipy.ndimage
import matplotlib
import math
matplotlib.use('TkAgg')
import matplotlib.pyplot as pyplot


def circleArray(r1, r2, weight, oval=0, ovalscaling= 2, invers=False, xshift = 1, yshift= 1):

    width =2*r2+1
    height=2*r2+1

    #vertical oval
    if oval == 1:
        width = math.ceil(width/ovalscaling)
    #horizontal oval
    if oval == -1:
        height = math.ceil(height/ovalscaling)

    a,b = math.floor(width/2), math.floor(height/2)

    map_ = [[ 0 for x in range(width)] for y in range(height)]

    innerV = 1
    outerV = weight
    if invers:
        innerV = weight
        outerV = 1

    ystart=0
    yend=height
    xstart=0
    xend=width
    if xshift == 1:
        xstart=a
    elif xshift == -1:
        xend=a
    if yshift ==1 :
        ystart=a
    elif yshift == -1:
        yend=a
    for y in range(ystart, yend):
        for x in range(xstart, xend):
            if abs((x-a)**2 + (y-b)**2) <= r1**2:
                map_[y][x] = innerV

            elif abs((x-a)**2 + (y-b)**2) <= r2**2:
                    map_[y][x] = outerV
    for line in map_:
        print (line)

    return map_

def squareArray(r1, r2, weight, rect=0, rectscaling= 2):

    width = 2*r2+1
    height=2*r2+1

    #vertical oval
    if rect == 1:
        width = math.ceil(width/rectscaling)

    #horizontal oval
    if rect == -1:
        height = math.ceil(height/rectscaling)

    a,b = math.floor(width/2), math.floor(height/2)

    map_ = [[ weight for x in range(height)] for y in range(width)]

    for y in range(height):
        for x in range(width):
            if abs(x-a) <= r1 and abs(y-a)<= r1:
                map_[y][x] = 1
    for line in map_:
        print (line)

    return map_

class Life(object):
    """Implements Conway's Game of Life.

    n:     the number of rows and columns
    """

    def __init__(self, n, mode='wrap', random = False, p=0.001, radius1 = 3, radius2 = 6, weight=-0.25, doubleStates = True, inversion = False):
        """Attributes:
        n:      number of rows and columns
        mode:   how border conditions are handled
        array:  the numpy array that contains the data.
        weights: the kernel used for convolution
        """
        self.n = n
        self.mode = mode
        self.radius2 = radius2
        self.doubleStates = doubleStates
        random = True
        if random:
            #self.array = numpy.random.random_integers(0, 1, (n, n))
            self.array=numpy.random.choice([0,1],(n,n), p=[1-p,p])
        else:
            self.array = numpy.zeros((n, n), numpy.int8)

        """self.weights = numpy.array([[ 0, 0, 0,-1, 0, 0, 0],
                                    [ 0, 0,-1, 1,-1, 0, 0],
                                    [ 0,-1, 1, 1, 1,-1, 0],
                                    [-1, 1, 1, 1, 1, 1,-1],
                                    [ 0,-1, 1, 1, 1,-1, 0],
                                    [ 0, 0,-1, 1,-1, 0, 0],
                                    [ 0, 0, 0,-1, 0, 0, 0]
                                    ])
                                    """
        self.weights = numpy.array(circleArray(radius1, radius2, weight))


    def add_glider(self, x=0, y=0):
        coords = [(0,1), (1,2), (2,0), (2,1), (2,2)]
        for i, j in coords:
            self.array[x+i, y+j] = 1

    def add_stuff(self, x=0, y=0):
        coords = [(0,0), (1,1), (2,1), (0,1), (0,1)]
        for i, j in coords:
            self.array[x+i, y+j] = 1

    def loop(self, steps=1):
        """Executes the given number of time steps."""
        [self.step() for i in xrange(steps)]

    def step(self):
        """Executes one time step."""
        con = scipy.ndimage.filters.convolve(self.array,
                                             self.weights,
                                             mode=self.mode, cval=0.0 )

        #boolean = (con==3) | (con==12) | (con==13)
        sideBorders = True
        topbotBorders = True
        if (not self.doubleStates):
            boolean = (con >= 1)
            for y in range(self.n):
                for x in range(self.n):
                    if sideBorders and ((x < self.radius2) or (x > self.n-self.radius2)):
                        boolean[y][x] = True
                    if topbotBorders and ((y < self.radius2) or (y > self.n-self.radius2)):
                        boolean[y][x] = True

            self.array = numpy.int8(boolean)
        else:
            print ("doublestates")
            values = [[ 0 for x in range(self.n)] for y in range(self.n)]
            for y in range(self.n):
                for x in range(self.n):
                    if sideBorders and ((x < self.radius2) or (x > self.n-self.radius2)):
                        values[y][x] = 0
                    if topbotBorders and ((y < self.radius2) or (y > self.n-self.radius2)):
                        values[y][x] = 0
                    elif con[y][x] >= 2*self.radius2:
                        values[y][x] = 2
                    elif con[y][x] >= 1:
                        values[y][x] = 1
                    elif con[y][x] <= -(2*self.radius2):
                        values[y][x] = -1
            print(values)
            self.array = numpy.int8(values)


class LifeViewer(object):
    """Generates an animated view of the grid."""
    def __init__(self, life, cmap=matplotlib.cm.PuBuGn):
        self.life = life
        self.cmap = cmap

        self.fig = pyplot.figure()
        pyplot.axis([0, life.n, 0, life.n])
        pyplot.xticks([])
        pyplot.yticks([])

        self.pcolor = None
        self.update()

    def update(self):
        """Updates the display with the state of the grid."""
        if self.pcolor:
            self.pcolor.remove()

        a = self.life.array
        self.pcolor = pyplot.pcolor(a, cmap=self.cmap)
        self.fig.canvas.draw()

    def animate(self, steps=1):
        """Creates the GUI and then invokes animate_callback.

        Generates an animation with the given number of steps.
        """
        self.steps = steps
        self.fig.canvas.manager.window.after(1000, self.animate_callback)
        self.fig.savefig('/home/phill/Cell_Automata/test_0/pic.png')
        pyplot.show()

    def animate_callback(self):
        """Runs the animation."""
        for i in range(self.steps):
            self.life.step()
            self.fig.savefig('/home/phill/Cell_Automata/test_0/pic' + str(i)+ '.png')
            self.update()

        print ('end')
def main(script, n=20, radius1 = 5, radius2 = 10,p= 0.1, weight=-0.5, doubleStates=False, inversion=False, *args):


    n,radius1,radius2 = int(n),int(radius1), int(radius2)
    p,weight = float(p),float(weight)
    life = Life(n, random=True, radius1 = radius1, radius2 = radius2, p = p, weight = weight, doubleStates=doubleStates, inversion=inversion)
    #life.add_glider(10, 10)
    viewer = LifeViewer(life)
    viewer.animate(steps=10)

""" df """

if __name__ == '__main__':
    import sys

    profile = False
    if profile:
        import cProfile
        cProfile.run('main(*sys.argv)')
    else:
        main(*sys.argv)
