import numpy

def circleArray(r1=3, r2=4):


    width=2*r2 +1
    a, b = (r2) , (r2)

    map_ = [[' 0' for x in range(width)] for y in range(width)]
    print(a)
    # draw the circle
    for y in range(0,width):
        for x in range(0,width):
            if x == a and y == b:
                map_[y][x] = ' x'
            # see if we're close to (x-a)**2 + (y-b)**2 == r**2
            elif  abs((x-a)**2 + (y-b)**2) <= r1**2:
                            map_[y][x] = ' 1'

            elif  abs((x-a)**2 + (y-b)**2) <= r2**2:
                    map_[y][x] = '-1'

    # print the map
    for line in map_:
        print (' '.join(line))

def probArray(p,n):
    A = numpy.random.choice([0,1],p=[1-p,p])
    return A

print (circleArray())
